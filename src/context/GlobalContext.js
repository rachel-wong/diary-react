import { createContext, useReducer, useEffect } from 'react'
import AppReducer from './AppReducer'

// initial state is whatever entries that is in the localstorage, if not then an empty array
const initialState = {
  entries: localStorage.getItem("entries") !== undefined ? JSON.parse(localStorage.getItem("entries")) : []
}
// const initialState = {
//   entries: [
//     {
//       "id": 1,
//       "date": "20-02-2021",
//       "mood": "happy",
//       "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem."
//     },
// {
//       "id": 2,
//       "date": "10-03-2021",
//       "mood": "angry",
//       "text": "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Nulla porttitor accumsan tincidunt. Pellentesque in ipsum id orci porta dapibus. Nulla quis lorem ut libero malesuada feugiat."
//     },
// {
//       "id": 3,
//       "date": "15-04-2021",
//       "mood": "moody",
//       "text": "Proin eget tortor risus. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat."
//     },
//     {
//       "id": 4,
//       "date": "01-05-2021",
//       "mood": "neutral",
//       "text": "Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Proin eget tortor risus. Pellentesque in ipsum id orci porta dapibus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec rutrum congue leo eget malesuada."
//     }
//   ]
// }

export const GlobalContext = createContext(initialState)

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState)

  console.log(localStorage.getItem("entries"))

  // define actions here

  // create entry
  function addEntry(entry) {
    dispatch({
      type: "ADD_ENTRY",
      payload: entry
    })
  }

  // delete entry
  function deleteEntry(id) {
    dispatch({
      type: "DELETE_ENTRY",
      payload: id
    })
  }

  // on application load and whenever the state changes (add/delete), set the localstorage to whatever that is the (initial) state of entries
  useEffect(() => {
    localStorage.setItem("entries", JSON.stringify(state.entries))
  }, [state])

  return (
    <GlobalContext.Provider value={{ entries: state.entries, addEntry, deleteEntry }}>
      { children }
    </GlobalContext.Provider>
  )
}