import React, { useContext } from 'react'
import { Col } from 'react-bootstrap'
import { RiDeleteBin6Line } from 'react-icons/ri'
import { GlobalContext } from '../context/GlobalContext'
import styled from 'styled-components'
const Entry = ({ entry}) => {

  // get deleteEntry from context
  const { deleteEntry } = useContext(GlobalContext)

  const { id, text, mood, date } = entry
  let formattedDate = date.toLocaleString().split(",")[0]

  let moodColour = ""

  switch (mood) {
    case "Happy":
      moodColour = '#ffbe76'
      break;
    case "Confused":
      moodColour = '#95afc0'
      break;
    case "Annoyed":
      moodColour = '#6ab04c'
      break;
    case "Sad":
      moodColour = "#30336b"
      break;
    case "Angry":
      moodColour = "#ff7979"
      break;
    case "Bored":
      moodColour = "#22a6b3"
      break;
    default:
      return "#f9ca24"
  }

    const EntryNote = styled.div`
    background: ${moodColour};
    border-radius: 3rem;
    margin-left: 1rem !important;
    margin-right: 1rem !important;
  `

  return (
    <Col className="col-sm-5 entry p-5 mb-5">
      <p className="entry-text">{text}</p>
      <div className="entry-footer">
        <div className="entry-footer__label">
          <p className="entry-footer__label__mood">{mood}</p>
          <time className="entry-footer__label__date">{ formattedDate }</time>
        </div>
        <div className="entry-footer__controls">
          <button className="entry-footer__btn" onClick={ () => deleteEntry(id)}>
            <RiDeleteBin6Line />
          </button>
        </div>
      </div>
    </Col>
  )
}

export default Entry

