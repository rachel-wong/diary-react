import React, { useState, useContext } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { GlobalContext } from '../context/GlobalContext'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom';

// displays a form
const Add = () => {

  const { addEntry } = useContext(GlobalContext)
  const [date, setDate] = useState("")
  const [text, setText] = useState("")
  const [mood, setMood] = useState("")

  let history = useHistory();

    const onSubmit = (e) => {
      e.preventDefault()
      console.log("form", e, date, text, mood)
      // create an entry object
      let id = Math.floor(Math.random() * 10000000 ) + 1
      let newEntry = { id, text, mood, date}
      // pass it to addEntry which will modify the state by the AppReducer
      addEntry(newEntry)
      // empty the form
      setDate("")
      setText("")
      setMood("")
      history.push("/")
  }

  return (
    <div>
      <Container className="py-5">
        <Row>
          <h2 className="ml-auto mr-auto">Add entry</h2>
        </Row>
      </Container>
      <Container className="py-5 entry-form-wrapper">
        <form id="form" className="entry-form" onSubmit={ onSubmit }>
          <Container>
            <Row className="form-control">
              <label htmlFor="text">What happened today?</label>
              <textarea id="text" placeholder="Today this happened ..." value={text || ""} onChange={(e) => setText(e.target.value)}>
              </textarea>
            </Row>
            <Row className="form-control">
              <select value={mood || ""} onChange={(e) => setMood(e.target.value)}>
                <option value="Happy">Happy</option>
                <option value="Moody">Moody</option>
                <option value="Neutral">Neutral</option>
                <option value="Confused">Confused</option>
                <option value="Angry">Angry</option>
                <option value="Annoyed">Annoyed</option>
                <option value="Bored">Bored</option>
              </select>

              {/* <label htmlFor="mood">How do you feel about it?</label>
              <input type="text" id="mood" placeholder="Feels!" value={mood || ""} onChange={ (e) => setMood(e.target.value)}/> */}
            </Row>
            <Row className="form-control">
              <label htmlFor="date">When did this happen?</label>
              <DatePicker selected={date} onChange={(date) => setDate(date)} />
              {/* <input type="text" id="date" placeholder="Input a date" value={date || ""} onChange={ (e) => setDate(e.target.value)}/> */}
            </Row>
            <Row className="form-control">
              <button className="form-btn">Submit Entry </button>
            </Row>
          </Container>
        </form>
      </Container>
    </div>
  )
}

export default Add
