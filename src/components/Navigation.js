import React from 'react'
import { Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
const Navigation = () => {
  return (

    <Navbar bg="light" expand="lg" >
      <Container className="nav-container">
        <Link to="/">Diary</Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Nav className="ml-auto nav-links">
          <Link to="/">Home</Link>
          <Link to="/add">Add Entry</Link>
        </Nav>
      </Container>
  </Navbar>
  )
}

export default Navigation
